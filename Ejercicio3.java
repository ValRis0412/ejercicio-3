package ejercicio3;

public class Ejercicio3 {
	public static void main(String[] args) {
		System.out.println("Tecla de escape \t Significado");
		System.out.println("\n");
		System.out.println("\\n \t \t \t significa nueva linea");
		System.out.println("\\t \t \t \t Significa un tab de espacio");
		System.out.println("\\\" \t \t \t Es para poner \" (comillas dobles) dentro del texto, por ejemplo \"Belencita\" ");
		System.out.println("\\\\ \t \t \t Se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\\\' \t \t \t Se utiliza para las \' (comilla simple), para escribir por ejemplo \'Princesita\'");
	}
}
